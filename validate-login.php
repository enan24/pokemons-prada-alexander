<?php
require_once('connectionMysql.php');

session_start();

$username = isset($_POST["username"]) ? $_POST["username"] : '';
$password = isset($_POST["password"]) ? $_POST["password"] : '';

$mysql = new MYSQL();

$user = $mysql->searchUser($username);

if (!isset($user) || is_null($user) || $user->num_rows == 0){
  $error = md5('error_login');
}

$user = $user->fetch_assoc();

if ($user["password"] == md5($password)) {
  echo "Logueado";
  $_SESSION["isLogged"] = true;
  header("location: index.php");
  exit;
} else {
  $error = md5('error_password');
  header("location: login.php?error=" . $error);
}

?>
