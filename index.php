<?php
session_start();

require_once("connectionMysql.php");

$isLogged = !isset($_SESSION["isLogged"]) ? false : true;

$pse = isset($_GET["pse"]) ? $_GET["pse"] : '';
$error = isset($_GET["error"]) ? $_GET["error"] : '';
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="recursos/icons8-pokedex-48.png">
    <title>Pokedex - Inicio</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>
  <body>
    <nav class="navbar sticky-top navbar-light" style="background-color: #ff4949;">
      <a class="navbar-brand" href="index.php">
        <img src="recursos/pokeball.svg" width="100%" style="max-width: 40px">
      </a>
      <form class="my-2 my-lg-0" style="flex: 1; max-width: 600px" method="POST" action="search.php">
        <input class="form-control mr-sm-2 rounded" name="query" placeholder="Buscar">
      </form>
    <?php
      if (!$isLogged) {
        echo "
            <a class='navbar-brand' style='margin-left: 10px; margin-right: 0px' tabindex='-1' aria-disabled='true' data-toggle='modal' data-target='#modal'><i class='fas fa-sign-in-alt'></i></a>
          </nav>
          <div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
            <div class='modal-dialog' role='document'>
              <div class='modal-content'>
                <div class='modal-header'>
                  <h5 class='modal-title' id='exampleModalLabel'>Ingresar</h5>
                  <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                  </button>
                </div>
                <div class='modal-body'>
                  <form method='POST' action='validate-login.php'>
                    <div class='form-group'>
                      <label for='exampleInputEmail1'>Nombre de usuario</label>
                      <input type='text' class='form-control' id='username' name='username' aria-describedby='emailHelp' placeholder='Nombre de usuario'>
                    </div>
                    <div class='form-group'>
                      <label for='exampleInputPassword1'>Contraseña</label>
                      <input type='password' name='password' class='form-control' id='exampleInputPassword1' placeholder='Contraseña'>
                    </div>
                    <button type='submit' class='btn btn-primary' style='background-color: #ff4949; border-color: #ff4949'>Iniciar Sesión</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        ";
      } else {
        echo "
          <a class='navbar-brand' style='margin-left: 10px; margin-right: 0px' tabindex='-1' aria-disabled='true' href='signuot.php'><i class='fas fa-sign-out-alt'></i></a>
        </nav>
        <nav>
          <div class='nav nav-tabs' id='nav-tab' role='tablist'>
            <a class='nav-item nav-link active' id='nav-pokemons-tab' data-toggle='tab' href='#nav-pokemons' role='tab' aria-controls='nav-pokemons' aria-selected='true'>Lista Pokemons</a>
            <a class='nav-item nav-link' id='nav-create-tab' data-toggle='tab' href='#nav-create' role='tab' aria-controls='nav-create' aria-selected='false'>Ingresar Pokemon</a>
          </div>
        </nav>
        ";
      }
    ?>
    <br>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-pokemons" role="tabpanel" aria-labelledby="nav-home-tab">
        <?php
        if ($error == 'b31cea81aba661fa83b0df71aab0b8a8') {
          $error = "Se debe ingresar una imagen para el pokemon.";
        } else if ($error == 'b06d1db11321396efb70c5c483b11923') {
          $error = "Se debe ingresar un nombre para el pokemon.";
        } else if ($error == '57eb5fb7b8e11428ef0bf7a525dd8cc1') {
          $error = 'Ocurrió un error al eliminar el pokemon';
        }
        if ($error != '') {
          echo "
          <div class='alert alert-danger' role='alert'>
          ".$error."
          </div>
          ";
          $error = '';
        }
        if ($pse == 'cddbd88cf4a56b701fd827f8e4799fa7') {
          $pse = 'Se guardo el pokemon correctamente.';
        } else if ($pse == '34e5be96e25c7f65f947912d2481e707') {
          $pse = 'Se ha eliminado correctamente';
        }
        if ($pse != '') {
          echo "
          <div class='alert alert-success' role='alert'>
          ".$pse."
          </div>
          ";
          $pse = '';
        }
        ?>
        <div class="container">
          <div class="card-columns">
            <?php
            $mysql = new MYSQL();
            $result = $mysql->search('');
            if (!$result) {
              echo "<p>No se han encontrado Pokemons!</p>";
            } else {
              for($i = 0; $i < $result->num_rows; $i++) {
                $pokemon = $result->fetch_assoc();
                echo "
                <div class='card'>
                <img src='".$pokemon['image']."' class='card-img-top' alt='".$pokemon['name']."'>
                <div class='card-body'>
                  <h5 class='card-title'>".$pokemon['name']."</h5>
                ";
                if ((isset($pokemon['description'])) && !is_null($pokemon['description'])) {
                  echo "
                    <p class='card-text'>".$pokemon['description']."</p>
                  ";
                }
                echo "
                </div>
                <ul class='list-group list-group-flush'>
                ";
                if ((isset($pokemon['type'])) && !is_null($pokemon['type'])) {
                  echo "
                  <li class='list-group-item'>
                  <p class='card-text'>Tipo: ".$pokemon['type']."</p>
                  </li>
                  ";
                }
                if ((isset($pokemon['skill'])) && !is_null($pokemon['skill'])) {
                  echo "
                  <li class='list-group-item'>
                  <p class='card-text'>Habilidad: ".$pokemon['skill']."</p>
                  </li>
                  ";
                }
                echo "
                </ul>
                ";
                if ($isLogged) {
                  echo "
                    <div class='card-footer text-muted' style='text-align: right'>
                      <a class='btn btn-primary' style='background-color: #ff4949; border-color: #ff4949' href='update.php?id=".$pokemon['id']."'>Modificar</a>
                      <a class='btn btn-danger'  href='remove.php?id=".$pokemon['id']."'>Eliminar</a>
                    </div>
                  ";
                }
                echo "
                </div>
                ";
              }
            }
            ?>
          </div>
        </div>
        <br>
      </div>
      <div class="tab-pane fade" id="nav-create" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="container" style="max-width:400px;magin: auto">
          <br>
          <form method="POST" action="validate-pokemon.php" enctype="multipart/form-data">
            <div class="form-group">
              <label for="exampleInputEmail1">Nombre del Pokemon</label>
              <input type="text" class="form-control" id="username" name="name" aria-describedby="emailHelp" placeholder="Nombre del pokemon" required>
              <div class="valid-feedback">
                Looks good!
              </div>
            </div>
            <div class="form-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="validatedCustomFile" name="image_pokemon" required>
                <label class="custom-file-label" for="validatedCustomFile" data-browse="Buscar">Eliga una imagen</label>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleFormControlTextarea1">Descripcion</label>
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description"></textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Habilidad</label>
              <input type="text" class="form-control" id="username" name="skill" placeholder="Ingrese la habilidad">
            </div>
            <div class="form-group">
              <select class="custom-select" name="type">
                <option value="Normal" selected>Normal</option>
                <option value="Lucha">Lucha</option>
                <option value="Volador">Volador</option>
                <option value="Veneno">Veneno</option>
                <option value="Tierra">Tierra</option>
                <option value="Roca">Roca</option>
                <option value="Bicho">Bicho</option>
                <option value="Fantasma">Fantasma</option>
                <option value="Acero">Acero</option>
                <option value="Fuego">Fuego</option>
                <option value="Bicho">Agua</option>
                <option value="Planta">Planta</option>
                <option value="Electrico">Electrico</option>
                <option value="Psiquico">Psiquico</option>
                <option value="Hielo">Hielo</option>
                <option value="Dragon">Dragon</option>
                <option value="Hada">Hada</option>
                <option value="Siniestro">Siniestro</option>
              </select>
            </div>
            <div style="text-align:right">
              <button type="submit" class="btn btn-primary" style="background-color: #ff4949; border-color: #ff4949">Guardar</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
      });
    </script>
  </body>
</html>
