<?php
  require_once("connectionMysql.php");

  $mysql = new MYSQL();

  if (!isset($_GET["id"]) || is_null($_GET["id"])) {
    header("location: index.php?error=".md5('missing_id'));
    exit;
  }

  $mysql->deletePokemon($_GET["id"]);
?>
