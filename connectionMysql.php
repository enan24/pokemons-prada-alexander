<?php
class MYSQL
{
  private $conexion;

  function __construct()
  {
    $this->conexion = new mysqli('localhost', 'root', '', 'pokemonsPradaAlexander');
    if($this->conexion->connect_error) {
    	die("Ha tenido un error de conexion");
    }
  }

  function searchUser($username) {
    return $this->conexion->query('select * from user where username = "' . $username . '";');
  }

  function search($search, $id = -1) {
    $sql = "select * from pokemon;";
    if ($search != '') {
      $sql = 'select * from pokemon where name = "%'.$search.'%";';
    }
    if ($id != -1) {
      $sql = 'select * from pokemon where id = "'.$id.'";';
    }
    return $this->conexion->query($sql);
  }

  function deletePokemon($pokemon) {
    $result = $this->conexion->query('delete from pokemon where id = ' . $pokemon . ';');
    if ($result) {
      $pse = md5("Eliminado con exito");
      header("location: index.php?pse=".$pse);
      exit;
    } else {
      $error = md5("Error al eliminar");
      header("location: index.php?pse=".$error);
      exit;
    }
  }

  function createPokemon($image, $name, $type, $skill, $description) {
    if (!isset($image) || is_null($image)) {
      return 'missing_image';
    }
    if (!isset($name) || is_null($name)) {
      return 'missing_name';
    }
    if (!isset($type) || is_null($type)) {
      $skill = '';
    }
    if (!isset($skill) || is_null($skill)) {
      $skill = '';
    }
    if (!isset($description) || is_null($description)) {
      $description = '';
    }
    $result = $this->conexion->query('insert into pokemon (image, name, skill, type, description) values ("'. $image .'","'. $name .'","'. $skill .'","'. $type .'","'. $description .'");');
    if ($result) {
      $pse = md5("Se guardo el pokemon correctamente");
      header("location: index.php?pse=".$pse);
      exit;
    } else {
      $error = md5("Error al guardar");
      header("location: index.php?pse=".$error);
      exit;
    }
  }

  function updatePokemon($id, $image, $name, $type, $skill, $description) {
    $result = $this->conexion->query('update pokemon set image="'.$image.'", name="'.$name.'", type="'.$type.'", skill="'.$skill.'", description="'.$description.'" where id="'.$id.'";');
    if ($result) {
      $pse = md5("Se guardo el pokemon correctamente");
      header("location: index.php?pse=".$pse);
      exit;
    } else {
      $error = md5("Error al guardar");
      header("location: index.php?pse=".$error);
      exit;
    }
  }
}

?>
