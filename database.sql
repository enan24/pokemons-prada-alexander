drop database if exists pokemonsPradaAlexander;

create database pokemonsPradaAlexander;

use pokemonsPradaAlexander;

create table user(
  id int PRIMARY KEY auto_increment,
  username varchar(255) NOT NULL,
  password varchar(255) NOT NULL
);

create table pokemon(
  id int PRIMARY KEY auto_increment,
  image varchar(255) NOT NULL,
  name varchar(255) NOT NULL,
  skill varchar(255),
  type varchar(255),
  description varchar(255)
);

insert into user (username, password) values('enan24', '8d7e9d169108faad4c0463a20472f6f5');

insert into pokemon (image, name, skill, type) values
  ('recursos/Pikachu.png', 'Pikachu', 'Electricidad estatica', 'Electrico'),
  ('recursos/Bulbasaur.png', 'Bulbasaur', 'Espesura', 'Planta'),
  ('recursos/Caterpie.png', 'Caterpie', 'Polvo escudo', 'Bicho'),
  ('recursos/Charmander.png', 'Charmander', 'Mar llamas', 'Fuego'),
  ('recursos/Squirtle.png', 'Squirtle', 'Torrente', 'Agua'),
  ('recursos/Metapod.png', 'Metapod', 'Mudar', 'Bicho'),
  ('recursos/Pidgey.png', 'Pidgey', 'Vista lince', 'Normal');
