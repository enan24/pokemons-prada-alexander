<?php
require_once("connectionMysql.php");

$idPokemon = isset($_GET["id"]) ? $_GET["id"] : '';
if ($idPokemon == '') {
  header("location: index.php");
  exit;
} else {
  $mysql = new MYSQL();
  $result = $mysql->search('', $idPokemon);
  $result = $result->fetch_assoc();
  if (isset($_POST["name"])) {
    $name = isset($_POST["name"]) ? $_POST["name"] : '';
    $skill = isset($_POST["skill"]) ? $_POST["skill"] : '';
    $description = isset($_POST["description"]) ? $_POST["description"] : '';
    $type = isset($_POST["type"]) ? $_POST["type"] : '';
    $image = isset($_FILES['image_pokemon']['name']) ? $_FILES['image_pokemon']['name'] : '';

    if ($image == '') {
      $image = $result["image"];
    }

    if ($image != $result["image"]) {
      $image_name=$_FILES['image_pokemon']['name'];
      $temp = explode(".", $image_name);
      $newfilename = round(microtime(true)) . '.' . end($temp);
      $image="recursos/".$newfilename;
      move_uploaded_file($_FILES["image_pokemon"]["tmp_name"],$image);
    }

    $mysql->updatePokemon($idPokemon, $image, $name, $type, $skill, $description);
    exit;
  }
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Modificar - <?php echo $result["name"]; ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>
  <body>
    <div class='container' style='max-width:400px;magin: auto'>
      <br>
      <form method='POST' action='<?php echo "update.php?id=".$idPokemon; ?>' enctype='multipart/form-data'>
        <div class='form-group'>
          <label for='exampleInputEmail1'>Nombre del Pokemon</label>
          <input type='text' class='form-control' id='username' name='name' aria-describedby='emailHelp' placeholder='Nombre del pokemon' value='<?php echo $result['name']; ?>' required>
        </div>
        <div class='form-group'>
          <label>Imagen</label>
          <img src='<?php echo $result['image']; ?>' class='card-img-top'>
          <div class='custom-file'>
            <input type='file' class='custom-file-input' id='validatedCustomFile' name='image_pokemon'>
            <label class='custom-file-label' for='validatedCustomFile' data-browse='Buscar'>Eliga una imagen</label>
          </div>
        </div>
        <div class='form-group'>
          <label for='exampleFormControlTextarea1'>Descripcion</label>
          <textarea class='form-control' id='exampleFormControlTextarea1' rows='3' name='description' value='<?php echo $result['description']; ?>'></textarea>
        </div>
        <div class='form-group'>
          <label for='exampleInputEmail1'>Habilidad</label>
          <input type='text' class='form-control' id='username' name='skill' placeholder='Ingrese la habilidad' value='<?php echo $result['skill']; ?>'>
        </div>
        <div class='form-group'>
          <select class='custom-select' name='type'>
            <option value='<?php echo $result['type']; ?>' selected><?php echo $result['type']; ?></option>
            <option value='Normal'>Normal</option>
            <option value='Lucha'>Lucha</option>
            <option value='Volador'>Volador</option>
            <option value='Veneno'>Veneno</option>
            <option value='Tierra'>Tierra</option>
            <option value='Roca'>Roca</option>
            <option value='Bicho'>Bicho</option>
            <option value='Fantasma'>Fantasma</option>
            <option value='Acero'>Acero</option>
            <option value='Fuego'>Fuego</option>
            <option value='Bicho'>Agua</option>
            <option value='Planta'>Planta</option>
            <option value='Electrico'>Electrico</option>
            <option value='Psiquico'>Psiquico</option>
            <option value='Hielo'>Hielo</option>
            <option value='Dragon'>Dragon</option>
            <option value='Hada'>Hada</option>
            <option value='Siniestro'>Siniestro</option>
          </select>
        </div>
        <div style='text-align:right'>
          <button type='submit' class='btn btn-primary' style='background-color: #ff4949; border-color: #ff4949'>Guardar</button>
        </div>
      </form>
    </div>

    <script type="text/javascript">
      $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
      });
    </script>
  </body>
</html>
