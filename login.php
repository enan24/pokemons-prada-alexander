<?php
session_start();

if (isset($_SESSION["isLogged"]) && $_SESSION["isLogged"]) {
  header("location: index.php");
  exit;
}

$error = isset($_GET["error"]) ? $_GET["error"] : '';
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <title>Pokedex - Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="recursos/icons8-open-pokeball-48.png">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>
  <body>
    <nav class="navbar sticky-top navbar-light" style="background-color: #ff4949;">
      <a class="navbar-brand" href="index.php">
        <img src="recursos/pokeball.svg" width="100%" style="max-width: 40px">
      </a>
      <form class="my-2 my-lg-0" style="flex: 1; max-width: 600px" method="POST" action="search.php">
        <input class="form-control mr-sm-2 rounded" placeholder="Buscar">
      </form>
      <a class="navbar-brand"></a>
    </nav>
    <div class="container" style="max-width:400px;magin: auto">
      <br>
      <form method="POST" action="validate-login.php">
        <div class="form-group">
          <label for="exampleInputEmail1">Nombre de usuario</label>
          <input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp" placeholder="Nombre de usuario">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Contraseña</label>
          <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña">
        </div>
        <div style="text-align:right">
          <button type="submit" class="btn btn-primary" style="background-color: #ff4949; border-color: #ff4949">Iniciar sesión</button>
        </div>
      </form>
      <?php
      switch ($error) {
        case '041fa7462efc33bdb13cfdf7705ec3dc':
        $error = 'Ocurrió un error al iniciar sesión, vuelva a intentarlo.';
        break;
        case 'c7b4b0a7b72490a2d39a9aaeaa02a26a':
        $error = 'Usuario o contraseña incorrecta.';
        break;
        default:
        $error = '';
        break;
      }
      if ($error != '') {
        echo "
        <div class='alert alert-danger' role='alert'>
        ". $error ."
        </div>
        ";
      }
      ?>
    </div>
  </body>
</html>
