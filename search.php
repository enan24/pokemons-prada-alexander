<?php

session_start();

require_once("connectionMysql.php");

$isLogged = !isset($_SESSION["isLogged"]) ? false : true;

$mysql = new MYSQL();
$error = '';
$query = isset($_POST["query"]) ? $_POST["query"] : '';
$result = $mysql->search($query);
if ($result->num_rows == 0) {
  $error = "No se ha encontrado el Pokemon ". $query;
  $result = $mysql->search('');
}

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="recursos/icons8-smartphone-48.png">
    <title>Pokedex - Busqueda</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>
  <body>
    <nav class="navbar sticky-top navbar-light" style="background-color: #ff4949;">
      <a class="navbar-brand" href="index.php">
        <img src="recursos/pokeball.svg" width="100%" style="max-width: 40px">
      </a>
      <form class="my-2 my-lg-0" style="flex: 1; max-width: 600px" method="POST" action="search.php">
        <input class="form-control mr-sm-2 rounded" name="query" placeholder="Buscar">
      </form>
      <?php
        if (!$isLogged) {
          echo "
              <a class='navbar-brand' style='margin-left: 10px; margin-right: 0px' tabindex='-1' aria-disabled='true' data-toggle='modal' data-target='#modal'><i class='fas fa-sign-in-alt'></i></a>
            </nav>
            <div class='modal fade' id='modal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
              <div class='modal-dialog' role='document'>
                <div class='modal-content'>
                  <div class='modal-header'>
                    <h5 class='modal-title' id='exampleModalLabel'>Ingresar</h5>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                      <span aria-hidden='true'>&times;</span>
                    </button>
                  </div>
                  <div class='modal-body'>
                    <form method='POST' action='validate-login.php'>
                      <div class='form-group'>
                        <label for='exampleInputEmail1'>Nombre de usuario</label>
                        <input type='text' class='form-control' id='username' name='username' aria-describedby='emailHelp' placeholder='Nombre de usuario'>
                      </div>
                      <div class='form-group'>
                        <label for='exampleInputPassword1'>Contraseña</label>
                        <input type='password' name='password' class='form-control' id='exampleInputPassword1' placeholder='Contraseña'>
                      </div>
                      <button type='submit' class='btn btn-primary' style='background-color: #ff4949; border-color: #ff4949'>Iniciar Sesión</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          ";
        } else {
          echo "
            <a class='navbar-brand' style='margin-left: 10px; margin-right: 0px' tabindex='-1' aria-disabled='true' href='signuot.php'><i class='fas fa-sign-out-alt'></i></a>
          </nav>
          ";
        }
      ?>
    </nav>
    <?php
      if ($error != '') {
        echo "
          <div class='alert alert-danger' role='alert'>
            ".$error."
          </div>
        ";
      }
    ?>
    <br>
    <div class="container">
      <div class="card-columns">
        <?php
          for($i = 0; $i < $result->num_rows; $i++) {
            $pokemon = $result->fetch_assoc();
            echo "
            <div class='card'>
            <img src='".$pokemon['image']."' class='card-img-top' alt='".$pokemon['name']."'>
            <div class='card-body'>
              <h5 class='card-title'>".$pokemon['name']."</h5>
            ";
            if ((isset($pokemon['description'])) && !is_null($pokemon['description'])) {
              echo "
                <p class='card-text'>".$pokemon['description']."</p>
              ";
            }
            echo "
            </div>
            <ul class='list-group list-group-flush'>
            ";
            if ((isset($pokemon['type'])) && !is_null($pokemon['type'])) {
              echo "
              <li class='list-group-item'>
              <p class='card-text'>Tipo: ".$pokemon['type']."</p>
              </li>
              ";
            }
            if ((isset($pokemon['skill'])) && !is_null($pokemon['skill'])) {
              echo "
              <li class='list-group-item'>
              <p class='card-text'>Habilidad: ".$pokemon['skill']."</p>
              </li>
              ";
            }
            echo "
            </ul>
            ";
            if ($isLogged) {
              echo "
                <div class='card-footer text-muted' style='text-align: right'>
                  <a class='btn btn-primary' style='background-color: #ff4949; border-color: #ff4949' href='update.php?id=".$pokemon['id']."'>Modificar</a>
                  <a class='btn btn-danger'  href='remove.php?id=".$pokemon['id']."'>Eliminar</a>
                </div>
              ";
            }
            echo "
            </div>
            ";
          }

        ?>
      </div>
    </div>

  </body>
</html>
